# WhatsDesk

WhatsDesk is a **unofficial** client of whatsapp

This project is only inserting an instance of Whatsapp Web in an electron app and adds/enables desktop notifications.

## Download

+ [![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/whatsdesk)

+ [Web .deb](https://zerkc.gitlab.io/whatsdesk/)

![Imgur](https://i.imgur.com/oWkBGZV.png)

# Build from sources
## Without docker
1. Install nodejs v.16+ and yarn 1.4+
2. Install dependencies by `yarn --frozen-lockfile` command
3. Exec `npm run build`

## With docker and compose
1. Install docker and docker compose
2. Exec `npm run build:docker` 

## Licences

This project uses app icon from [Thoseicons.com](https://thoseicons.com/) under
[Creative Commons Licence v3.0](https://creativecommons.org/licenses/by/3.0/) and modified tray icon from
[EDT.im](http://edt.im) under [Creative Commons Licence v2.5](https://creativecommons.org/licenses/by/2.5/).
